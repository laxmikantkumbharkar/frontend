import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})
export class AddCustomerComponent implements OnInit {
 
  Cname = ''; 
  Carea = '';
  Pin = null;
  Phone = null;
  Cemail = '';
  Cusername = '';
  Cpassword = '';
  Cgender = '';
  Cthumbnail = '';
  constructor(
    private router: Router,
    private customerService: CustomerService
  ) { }

  onAdd(){
      this.customerService
          .addCustomers(this.Cname, this.Carea,this.Pin,
            this.Phone,this.Cemail,this.Cusername,
            this.Cpassword,this.Cgender,this.Cthumbnail)
          .subscribe( response =>{
              console.log(response);

              this.router.navigate(['/customer-list']);
          });       
  }

  onCancel(){
    return this.router.navigate(['/customer-list']);
  }

  ngOnInit() {
  }

}
