import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  Pname = '';
  Prate = null;
  Pcategory = '';
  PQuantity = '';
  Pthumbnail = '';
  constructor(
    private router: Router,
    private productService: ProductService)
    { }

  ngOnInit() {
  }

  onAdd(){
    this.productService
        .addProducts(this.Pname,this.Prate,this.Pcategory,this.PQuantity,this.Pthumbnail)
        .subscribe(response =>{
          console.log(response);

          this.router.navigate(['/product-list'])
        });
  }

  onCancel(){
    this.router.navigate(['/product-list']);
  }

}
