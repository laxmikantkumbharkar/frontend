import { BrowserModule } from '@angular/platform-browser';
import { NgModule,Component } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

import { ProductService } from './product.service';
import { MilkRateService } from './milkrate.service';
import { CustomerService } from './customer.service';
import { DealerService } from './dealer.service';
import { StaffService } from './staff.service';
import { FeedbackService } from './feedback.service';
import { LoginService } from './login.service';
//Product realted component
import { ProductListComponent } from './product-list/product-list.component';
import { AddProductComponent } from './add-product/add-product.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { UpdateProductComponent } from './update-product/update-product.component';

import { LoginComponent } from './login/login.component';
//Milk rate related component
import { MilkRateListComponent } from './milk-rate-list/milk-rate-list.component';
import { AddMilkRateComponent } from './add-milk-rate/add-milk-rate.component';
//Customer realted component
import { CustomerListComponent } from './customer-list/customer-list.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
//Dealer realted component
import { DealerListComponent } from './dealer-list/dealer-list.component';
import { AddDealerComponent } from './add-dealer/add-dealer.component';
//Staff realted component
import { StaffListComponent } from './staff-list/staff-list.component';
import { AddStaffComponent } from './add-staff/add-staff.component';
//Feedback realted component
import { FeedbackListComponent } from './feedback-list/feedback-list.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';

// decorator
// metadata
@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    AddProductComponent,
    UpdateProductComponent,
    MilkRateListComponent,
    LoginComponent,
    AddMilkRateComponent,
    CustomerListComponent,
    AddCustomerComponent,
    DealerListComponent,
    AddDealerComponent,
    StaffListComponent,
    AddStaffComponent,
    FeedbackListComponent,
    ProductDetailsComponent,
    CustomerDetailsComponent,
   
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      //Product related routers
      { path : 'product-list' ,component:ProductListComponent},
      { path : 'add-product' , component:AddProductComponent},
      { path : 'update-product' , component:UpdateProductComponent},
      { path : 'product-details' ,component:ProductDetailsComponent},
      //Milk rate related routers
      { path : 'milk-rate-list' , component:MilkRateListComponent},
      { path : 'add-milk-rate' , component:AddMilkRateComponent},
      //Login page related routers
      { path : 'app-login' , component: LoginComponent},
      //Customer related routers
      { path : 'customer-list' , component: CustomerListComponent},
      { path : 'add-customer' , component: AddCustomerComponent},
      { path : 'customer-details' , component: CustomerDetailsComponent},
      //Dealer related routers
      { path : 'dealer-list' , component: DealerListComponent},
      { path : 'add-dealer' , component: AddDealerComponent},
      //Staff related routers
      { path : 'staff-list' , component: StaffListComponent},
      { path : 'add-staff' , component: AddStaffComponent},
      //Feedback related routers
      { path : 'feedback-list' , component: FeedbackListComponent},
      //Login related routers
      { path : 'login' , component: LoginComponent },
    ])
  ],
  providers: [
    ProductService,
    MilkRateService,
    CustomerService,
    DealerService,
    StaffService,
    FeedbackService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
