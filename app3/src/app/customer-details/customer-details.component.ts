import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {

  selectedCustomer = {};
  constructor(
    private activatedRoute: ActivatedRoute,
    private customerService: CustomerService  ) { 
        this.activatedRoute.queryParams.subscribe(params => {
          const Cid = params['Cid'];

          this.customerService
              .getCustomerDetails(Cid)
              .subscribe(response => {
                const result = response.json();
                this.selectedCustomer = result.data;
              });
        });
    }

  ngOnInit() {
  }

}
