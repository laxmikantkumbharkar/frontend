import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  Customer = [];
  Cname = ''; 
  Carea = '';
  Pin = null;
  Phone = null;
  Cemail = '';
  Cusername = '';
  Cpassword = '';
  Cgender = '';
  Cthumbnail = '';
  constructor(
    private router: Router,
    private customerService: CustomerService ) { 
        this.refreshCustomerList();
    }

    refreshCustomerList(){
      this.customerService
          .getCustomers()
          .subscribe(response =>{
              const result = response.json();
              console.log(result);
              this.Customer=result.data;
          });
    }

    onDetails(customer){
      this.router.navigate(['/customer-details'],{ queryParams: {Cid:customer.Cid}});
    }

    onDelete(customer){
      const answer = confirm('Are you sure want to delete '+ customer.Cname+ ' ?');
      if(answer){
        this.customerService
            .deleteCustomer(customer.Cid)
            .subscribe(response =>{
                const result = response.json();
                console.log(result);
                this.refreshCustomerList();
            });
      }
    }

    onUpdate(customer){
      this.router.navigate(['update-customer'],{queryParams: {Cid:customer.Cid}});
    }

  ngOnInit() {
  }

}
