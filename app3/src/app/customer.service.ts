import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';

@Injectable()
export class CustomerService {
    url = 'http://localhost:9000/Customer';

    constructor(private http: Http){

    }

    getCustomers(){
        return this.http.get(this.url);
    }

    addCustomers(
        Cname:string,Carea:string,Pin:number,
        Phone:number,Cemail:string,Cusername:string,
        Cpassword:string,Cgender:string,Cthumbnail:string
    ){

        const body={
            Cname:Cname, Carea:Carea, Pin:Pin,
            Phone:Phone, Cemail:Cemail, Cusername:Cusername,
            Cpassword:Cpassword, Cgender:Cgender ,Cthumbnail:Cthumbnail
        }

        const header = new Headers({'Content-Type': 'application/json'});
        const requestOption = new RequestOptions({headers: header});
        return this.http.post(this.url,body,requestOption);
    }

    deleteCustomer(Cid: number){
        return this.http.delete(this.url + '/' + Cid);
    }

    getCustomerDetails(Cid: number){
        return this.http.get(this.url + '/' + Cid);
    }

    updateCustomer(
        Cname:string,Carea:string,Pin:number,
        Phone:number,Cemail:string,Cusername:string,
        Cpassword:string,Cgender:string,Cthumbnail:string
    ){
        const body={
            Cname:Cname, Carea:Carea, Pin:Pin,
            Phone:Phone, Cemail:Cemail, Cusername:Cusername,
            Cpassword:Cpassword, Cgender:Cgender, Cthumbnail:Cthumbnail
        };

        const header = new Headers({'Content-Type': 'application/json'});
        const requestOption = new RequestOptions({ headers: header});

        return this.http.put(this.url, body, requestOption);
    }
}