import { Injectable } from "@angular/core";
import { Http, RequestOptions, Headers } from '@angular/http';


@Injectable()
export class FeedbackService{

    url = 'http://localhost:9000/feedback';

    constructor(private http: Http ){

    }

    getFeedback(){
        return this.http.get(this.url);
    }

    addFeedbackResponse(
        Name:string, Address: string, Taluka: string,
        District: string, Mobile: string, Email: string,
        fb: string, date : Date, Response: string
    ){
        const body = {
            Name: Name, Address: Address, Taluka: Taluka,
            District: District, Mobile: Mobile, Email: Email,
            fb: fb, date: date, Response: Response
        }

        const header = new Headers({'Content-Type' : 'application/json'});
        const requestOption = new RequestOptions({ headers : header });

        return this.http.post(this.url, body, requestOption);
    }

    updateFeedback(
        Name:string, Address: string, Taluka: string,
        District: string, Mobile: string, Email: string,
        fb: string, date : Date, Response: string
    ){
        const body = {
            Name: Name, Address: Address, Taluka: Taluka,
            District: District, Mobile: Mobile, Email: Email,
            fb: fb, date: date, Response: Response
        }

        const header = new Headers({'Content-Type' : 'application/json'});
        const requestOption = new RequestOptions({ headers : header });

        return this.http.post(this.url, body, requestOption);
    }

    deleteFeedback(){
        // return this.http.delete(this.url + '/' + );
    }
}