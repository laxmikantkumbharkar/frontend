import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { Router, Route } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  Cusername = '';
  Cpassword = '';
  constructor(
    private router: Router,
    private loginService: LoginService) { }

  ngOnInit() {
  }

  onLogin(){
    this.loginService
        .signin(this.Cusername,this.Cpassword)
        .subscribe(response =>{
          const result = response.json();
          if(result.status == 'error'){
            alert('invalid username or password')
          }else{
            sessionStorage['login_status'] = '1';
            alert('Welcome to product list page');
            this.router.navigate(['/milk-rate-list'])
          }
        })
  }

  onCancel(){
    this.router.navigate(['/product-list'])
  }

}
