import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class MilkRateService {

  url = 'http://localhost:9000/milkrate';

  constructor(private http: Http) {

  }

  //Products Services
  //================================================================
  getMilkrate(){
    return this.http.get(this.url);
  }

  addMilkrate(
    Fat:number,Buffalo:number,Cow:number){
    
      const body={
        Fat:Fat,
        Buffalo:Buffalo,
        Cow:Cow,
      };

      const header = new Headers({ 'Content-Type': 'application/json' });
      const requestOption = new RequestOptions({ headers: header });

      return this.http.post(this.url, body, requestOption);
  }

  deleteMilkrate(Fat:number){
    return this.http.delete(this.url + '/' + Fat);
  }


  //   getProductDetails(Pid:number){
//     return this.http.get(this.url + '/'+Pid);
//   }

//   updateProducts(
//     Pname:String,Prate:number,Pcategory:String,PQuantity:String){
    
//       const body={
//         Pname:Pname,
//         Prate:Prate,
//         Pcategory:Pcategory,
//         PQuantity:PQuantity
//       };

//       const header = new Headers({ 'Content-Type': 'application/json' });
//       const requestOption = new RequestOptions({ headers: header });
//       return this.http.put(this.url, body, requestOption);
//   }

}