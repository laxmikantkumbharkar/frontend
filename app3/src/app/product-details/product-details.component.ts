import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  selectedProduct = {};   
  // Pid = null;
  // Pname = '';
  // Prate = null;
  // Pcategory = '';
  // PQuantity = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private productService: ProductService ) { 
      
            this.activatedRoute.queryParams.subscribe(params => {
              const Pid = params['Pid'];

              this.productService
                .getProductDetails(Pid)
                .subscribe(response => {
                  const result = response.json();
                  //console.log(result);
                  this.selectedProduct=result.data;
                });
            }); 
  }

  ngOnInit() {
  }

}
