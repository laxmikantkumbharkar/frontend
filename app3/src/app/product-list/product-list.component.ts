import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Router } from '@angular/router'

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  Products = [];   //db table name
  Pname = '';
  Prate = null;
  Pcategory = '';
  PQuantity = '';
  Pthumbnail = '';

  constructor(
    private router: Router,
    private productService: ProductService) {
      this.refreshProductList();
     }

     refreshProductList(){
       this.productService
           .getProducts()
           .subscribe(response => {
             const result = response.json();
             console.log(result);
             this.Products=result.data;
           });
     }

     onDetails(product){
       this.router.navigate(['/product-details'],{ queryParams: {Pid:product.Pid}});
     }

     onDelete(product){
        const answer = confirm('Are you sure want to delete '+product.Pname+ ' ?');
        if(answer){
          this.productService.deleteProducts(product.Pid)
              .subscribe(response => {
                const result = response.json();
                console.log(result);
                this.refreshProductList();
              });
        }
    }

    onUpdate(product){
      this.router.navigate(['/update-product'],{ queryParams: {Pid:product.Pid}});
    };

  ngOnInit() {
  }

  }

