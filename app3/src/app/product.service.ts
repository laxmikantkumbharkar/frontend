import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ProductService {

  url = 'http://localhost:9000/product';

  constructor(private http: Http) {

  }

  //Products Services
  //================================================================
  getProducts(){
    return this.http.get(this.url);
  }

  getProductDetails(Pid:number){
    console.log(this.url+ '/' + Pid);
    return this.http.get(this.url + '/'+ Pid);
  }

  addProducts(
    Pname:String,Prate:number,Pcategory:String,PQuantity:String,Pthumbnail: String){
    
      const body={
        Pname:Pname,
        Prate:Prate,
        Pcategory:Pcategory,
        PQuantity:PQuantity,
        Pthumbnail:Pthumbnail
      };

      const header = new Headers({ 'Content-Type': 'application/json' });
      const requestOption = new RequestOptions({ headers: header });

      return this.http.post(this.url, body, requestOption);
  }

  deleteProducts(Pid:number){
    return this.http.delete(this.url + '/' + Pid);
  }

  updateProducts(
    Pid:number,Pname:String,Prate:number,Pcategory:String,PQuantity:String,Pthumbnail:String){
    
      const body={
        Pid: Pid,
        Pname:Pname,
        Prate:Prate,
        Pcategory:Pcategory,
        PQuantity:PQuantity,
        Pthumbnail:Pthumbnail
      };

      const header = new Headers({ 'Content-Type': 'application/json' });
      const requestOption = new RequestOptions({ headers: header });
      return this.http.put(this.url+'/'+Pid, body, requestOption);
  }

}