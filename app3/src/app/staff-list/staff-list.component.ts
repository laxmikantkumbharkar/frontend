import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StaffService } from '../staff.service';

@Component({
  selector: 'staff-list',
  templateUrl: './staff-list.component.html',
  styleUrls: ['./staff-list.component.css']
})
export class StaffListComponent implements OnInit {

  Staff = [];               Sid = null; 
  Sname = '';               Saddress = '';
  Semail = '';              DeptId = null;  
  Srole = '';               Ssalary = null; 
  Susername = '';           Spassword = '';
  Sgender = '';             Sage = null;  
  Sphone = '';
  
  constructor(
    private router: Router,
    private staffService: StaffService ) { 
        this.refreshStaffList();
    }

    refreshStaffList(){
        this.staffService
            .getStaffList()
            .subscribe( response =>{
                const result = response.json();
                console.log(result);
                this.Staff = result.data;
            });
    }

    onDetails(staff){
        this.router.navigate(['/staff-details'], { queryParams : { Sid:staff.Sid}});
    }

    onDelete(staff){
        const answer = confirm('Are you sure want to delete '+ staff.Sid + ' ?');
        if(answer){
            this.staffService
                .deleteStaff(staff.Sid)
                .subscribe( response => {
                    const result = response.json();
                    console.log(result);

                    this.refreshStaffList();
                });
        }
    }

    onUpdate(staff){
        this.router.navigate(['update-staff'],{ queryParams: { Sid: staff.Sid }});
    }

  ngOnInit() {
  }

}
