import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {
  selectedProduct = {};
  Products = []
  Pid = null;
  Pname = '';
  Prate = null;
  Pcategory = '';
  PQuantity = '';
  Pthumbnail = '';
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private productService: ProductService) { 
      this.activatedRoute.queryParams.subscribe(params => {
        const Pid = params['Pid'];

        this.productService
          .getProductDetails(Pid)
          .subscribe(response => {
            const result = response.json();
            //console.log(result);
            this.selectedProduct=result.data;
          });
      }); 
    }

  ngOnInit() {
  }

  onUpdate(){
    this.productService
        .updateProducts(this.Pid,this.Pname,this.Prate,this.Pcategory,this.PQuantity,this.Pthumbnail)
        .subscribe(response =>{
          console.log(response);
          this.router.navigate(['/product-list']);
        });
  }

  onCancel(){
    this.router.navigate(['/product-list']);
  }

}
